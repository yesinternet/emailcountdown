<?php

define('VERSION', '1.0.1');

define('TIMEZONE', 'Europe/London');
define('IMAGE_PATH', "images/black.png");

$font = array(
    'size' => 60, // Font size, in pts usually.
    'angle' => 0, // Angle of the text
    'x-offset' => 20, // The larger the number the further the distance from the left hand side, 0 to align to the left.
    'y-offset' => 100, // The vertical alignment, trial and error between 20 and 60.
    'file' => __DIR__ . DIRECTORY_SEPARATOR . 'Futura.ttc', // Font path
    'R' => 255,
    'G' => 255,
    'B' => 255 // RGB Colour of the text
);

define('FONT', serialize($font));