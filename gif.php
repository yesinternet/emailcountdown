<?php

include 'config.php';
$timezone = defined('TIMEZONE') ? TIMEZONE : 'Europe/London';
$image_file = defined('IMAGE_PATH') ? IMAGE_PATH : 'images/countdown.png';

//Leave all this stuff as it is
date_default_timezone_set($timezone);
include 'GIFEncoder.class.php';
include 'php52-fix.php';

$time = $_GET['time'];
$future_date = new DateTime(date('r', strtotime($time)));
$time_now = time();
$now = new DateTime(date('r', $time_now));
$frames = array();
$delays = array();


// Your image link
$image = imagecreatefrompng($image_file);

$delay = 100;// milliseconds

if (defined('FONT')) {
    $font = unserialize(FONT);
} else {
    $font = array(
        'size' => 13, // Font size, in pts usually.
        'angle' => 0, // Angle of the text
        'x-offset' => 7, // The larger the number the further the distance from the left hand side, 0 to align to the left.
        'y-offset' => 30, // The vertical alignment, trial and error between 20 and 60.
        'file' => __DIR__ . DIRECTORY_SEPARATOR . 'Futura.ttc', // Font path
        'R' => 55,
        'G' => 160,
        'B' => 130 // RGB Colour of the text
    );
}
$font['color'] = imagecolorallocate($image, $font['R'], $font['G'], $font['B']);

for ($i = 0; $i <= 60; $i++) {

    $interval = date_diff($future_date, $now);

    if ($future_date <= $now) {
        // Open the first source image and add the text.
        $image = imagecreatefrompng($image_file);

        $text = $interval->format('00:00:00:00');
        imagettftext($image, $font['size'], $font['angle'], $font['x-offset'], $font['y-offset'], $font['color'], $font['file'], $text);
        ob_start();
        imagegif($image);
        $frames[] = ob_get_contents();
        $delays[] = $delay;
        $loops = 1;
        ob_end_clean();
        break;
    } else {
        // Open the first source image and add the text.
        $image = imagecreatefrompng($image_file);

        $days = str_pad($interval->format("%a"), 2, '0', STR_PAD_LEFT);
        $text = $interval->format("$days:%H:%I:%S");
        imagettftext($image, $font['size'], $font['angle'], $font['x-offset'], $font['y-offset'], $font['color'], $font['file'], $text);
        ob_start();
        imagegif($image);
        $frames[] = ob_get_contents();
        $delays[] = $delay;
        $loops = 0;
        ob_end_clean();
    }

    $now->modify('+1 second');
}

//expire this image instantly
header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', false);
header('Pragma: no-cache');
$gif = new AnimatedGif($frames, $delays, $loops);
$gif->display();
